import React, {useState, useMemo } from 'react';
import './App.css';
import RootTable from './components/RootTable';
import TablesContext from './TablesContext';

const App = () => {
  const tableJSON = require('./example-data.json')
  const objectPath = require("object-path");
  const originalHeaders = Object.keys(tableJSON[0].data).map(key => key)

  const [tablesData, setTablesData] = useState(tableJSON)
  const tables = useMemo(
    () => ({ tablesData, setTablesData}),
    [tablesData]
  )

  const [headerTitles, setHeaderTitles] = useState(originalHeaders)
  const headers = useMemo(
    () => ({ headerTitles, setHeaderTitles}),
    [headerTitles]
  )

  const deletePath = (path) => {
    console.log('nicepath',path)
    const copyObject = [...tablesData]
    console.log('pathdel', path)
    objectPath.del(copyObject,path)
    setTablesData(copyObject)
  }

  return (
    <>
    <div className="content">
    <TablesContext.Provider value={{tables, headers, deletePath}}>
       <RootTable rootData={tablesData}/>
    </TablesContext.Provider>
    </div>
    </>
  );
}

export default App;
