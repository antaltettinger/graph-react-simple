import React, {useState, useEffect, useContext, useCallback} from 'react';
import TableRow from './TableRow';
import NestedTable from './NestedTable';
import TableHeader from './TableHeader';
import TablesContext from '../TablesContext';

const RootTable= ({rootData}) => {
  const span = rootData.length
  const [isShowNested, setShowNested] = useState(new Map([]))
  const {tables, headers} = React.useContext(TablesContext)
  const tablesData = tables.tablesData
  const rootColumns = headers.headerTitles

  const toggleNested = (index) => {
    const isShowNestedNew = new Map(isShowNested)
    if(isShowNested.get(index)) {
      isShowNestedNew.set(index,false)
      setShowNested(isShowNestedNew)
    } else {
      isShowNestedNew.set(index,true)
      setShowNested(isShowNestedNew)
    }
  }

  return(
    <table className="root-table">
    <tbody>
        <TableHeader columns={rootColumns}/>
        {tablesData.map( (tablesData, index) => {
          let isOpen = false
          let hasKids = false
          const kidKey = Object.keys(tablesData.kids)[0]
          const kidData = tablesData.kids[kidKey]
          if(kidData !== undefined && kidData.records[0].hasOwnProperty('data')){
            isOpen = isShowNested.get(`${index}-${kidKey}`)
            hasKids = true
          }

          return(
          <React.Fragment key={index}>
          <TableRow records={tablesData.data} columns={rootColumns} hasKids={hasKids} toggleStatus={isOpen} clickCallback={() => toggleNested(`${index}-${kidKey}`)} path={index}/>
          { isOpen && hasKids &&
            <tr>
              <td colSpan={span}>
              <NestedTable nestedData={tablesData.kids} depth={1} isOpen={true} path={`${index}.kids`}/> 
              </td>
            </tr>
          }
          </React.Fragment>
          )})}
          </tbody>
    </table>
  )
  
}
export default RootTable