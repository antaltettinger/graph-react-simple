import React from 'react';
import { v4 as uuidv4 } from 'uuid';

const TableHeader= ({columns}) => {
  return(
      <tr>
      <td className="table-cell--utility">Toogle</td>
        {columns.map(columnName => ( 
          <th key={uuidv4()}>{columnName}</th>
        ))}
        <td className="table-cell--utility"></td>
      </tr>
  )
}
export default TableHeader