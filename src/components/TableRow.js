import React, {useState} from 'react';
import TablesContext from '../TablesContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight, faTrash, faMinus } from '@fortawesome/free-solid-svg-icons'
const TableRow = ({records, columns, hasKids, clickCallback, toggleStatus, path}) => {

    const {deletePath} = React.useContext(TablesContext)
  
   return(
     <>
      <tr>
      <td><FontAwesomeIcon icon={hasKids ? faAngleRight : faMinus} rotation={toggleStatus ? 90 : 0} onClick={clickCallback}/></td>
      {columns.map( column =>
        <td>
          {records[column]}
        </td>   
      )}
        <td><FontAwesomeIcon onClick={()=> deletePath(path)} icon={faTrash}/></td>
      </tr>
    </>
   )
}
export default TableRow