import React, { useState, useMemo, useCallback } from 'react';
import TableHeader from './TableHeader';
import TableRow from './TableRow';

const NestedTable = ({nestedData, depth, isOpen, path}) => {
  const tableName = Object.keys(nestedData)[0]
  const records = nestedData[tableName]['records']
  const columns = (records[0].hasOwnProperty('data')) ? Object.keys(records[0]['data']) : undefined
  //delete self if no records
  const currentPath = path + '.' + tableName + ".records"
  const [isShowKids, setShowKids] = useState(false) 
  const setOpenCallback = useCallback(() => {
    setShowKids(!isShowKids)
  }, [isShowKids])
  
  
  return (
    <>
    { (records.length !== 0 && columns !== undefined) &&
    <tr
    className="nested-table"
    style={{marginLeft: depth*30, display: isOpen ? 'block' : 'none'}}
    >
    {records.map((record, index) => (
      <>
      {index === 0 && <div>{tableName}</div>}
      <TableHeader columns={[...columns]}/>
      <TableRow records={record.data} columns={columns} clickCallback={() => setOpenCallback()} hasKids={Object.keys(record.kids).length} toggleStatus={isShowKids} path={`${currentPath}.${index}.data`}/>
       {(Object.keys(record.kids).length !== 0) && <NestedTable nestedData={record.kids} depth={depth++} isOpen={isShowKids} hasKids={true} path={`${currentPath}.kids`}/>
       }
      </>
    ))}
    </tr> 
    }
    </>
  )
  
}
export default NestedTable